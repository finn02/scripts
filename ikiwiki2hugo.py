#!/usr/bin/python3

'''convert an ikiwiki git repo into hugo'''

# Copyright (C) 2019 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Assertions:
# 1. all path names and file contents can be decoded in the current locale
# 2. only markdown files with a mdwn or md suffix are to be converted
#
# Inspired by https://blog.jak-linux.org/2018/10/25/migrated-website-from-ikiwiki-to-hugo/


import argparse
import logging
import os
import os.path
import pathlib
import re
import subprocess


def call(args):
    logging.debug('calling: %s', args)
    return subprocess.check_call(args)


def fake_call(args):
    logging.debug('would call: %s', args)


def parse_arguments():
    global call
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--repository', default=os.getcwd())
    parser.add_argument('--dry-run', '-n', action='store_true')
    parser.add_argument('--exclude', '-e', nargs='+',
                        default=['.git', '.ikiwiki', 'themes'])
    parser.add_argument('--test', '-t', action='store_true')
    args = parser.parse_args()
    if args.dry_run:
        call = fake_call
    args.repository = pathlib.Path(args.repository)
    return args


def git_move_all(source, destination):
    for path in source.iterdir():
        call(['git', 'mv', str(source / path), str(destination / path.name)])


def process_path(path, dryrun):
    # XXX: hardcoded (assumption 2)
    if path.suffix == '.mdwn':
        logging.debug('fixing suffix on %s', path)
        if not dryrun:
            call(['git', 'mv', str(path), str(path.with_suffix('.md'))])
            path = path.with_suffix('.md')
    # XXX: hardcoded (assumption 2)
    if path.suffix in ('.md', '.mdwn'):
        tmpfile = path.with_suffix('.md.tmp')
        logging.debug('rewriting source file: %s', path)
        with path.open() as source, tmpfile.open('w') as tmp:
            content = source.read()
            converted = ikiwiki2hugo(content)
            tmp.write(converted)
        if dryrun:
            tmpfile.unlink()
        else:
            tmpfile.replace(path)
            call(['git', 'add', str(path)])
    else:
        logging.info('not touching non-markdown file: %s', str(path))


META_RE = re.compile(r'\[\[!meta (\w+)="(?:"")?([^"[]*)(?:"")?"\]\]')


def ikiwiki2hugo(content):
    r'''convert given file content from ikiwiki to hugo

    >>> print(ikiwiki2hugo("foo\n"))
    foo
    <BLANKLINE>
    >>> data = """[[!meta title="foo"]]
    ... [[!meta date="2011-12-22T01:23:20-0500"]]
    ... [[!meta updated="2011-12-22T01:23:21-0500"]]
    ...
    ... foo
    ... """
    >>> print(ikiwiki2hugo(data))
    ---
    title: foo
    date: 2011-12-22T01:23:20-0500
    lastmod: 2011-12-22T01:23:21-0500
    ---
    <BLANKLINE>
    foo
    <BLANKLINE>
    '''
    match = META_RE.findall(content)
    metadata = {}
    if match:
        for name, value in match:
            if name == 'updated':
                name = 'lastmod'
            metadata[name] = value
    header = []
    for key, value in metadata.items():
        header.append("{}: {}".format(key, value))
    if header:
        header = "---\n" + "\n".join(header) + "\n---\n\n"
    else:
        header = ''
    content = META_RE.sub("", content)
    return header + content.lstrip()


def main():
    logging.basicConfig(level='DEBUG', format='%(levelname)s: %(message)s')
    args = parse_arguments()
    if args.test:
        import doctest
        doctest.testmod()
        return
    logging.warning('bisco rewrote this for Tails, consider using their version instead, see https://bisco.org/notes/converting-ikiwiki-to-hugo/')  # noqa: E501
    contentdir = args.repository / 'content'
    if contentdir.exists():
        logging.info('content dir already created, skipping mass move: %s', contentdir)
    else:
        logging.info('creating content dir: %s', contentdir)
        if not args.dry_run:
            contentdir.mkdir()
        logging.info('moving all files from %s to contentdir %s', args.repository, contentdir)
        git_move_all(args.repository, contentdir)
    if args.dry_run:
        contentdir = args.repository
    logging.info('walking entire content dir %s', contentdir)
    for root, dirs, files in os.walk(contentdir):
        for excluded_path in args.exclude:
            if excluded_path in dirs:
                logging.info('skipping ignored path %s', os.path.join(root, excluded_path))
                dirs.remove(excluded_path)
        for path in files:
            full_path = pathlib.Path(root) / path
            process_path(full_path, args.dry_run)


if __name__ == '__main__':
    main()
