#!/bin/sh

MODE="${1:-pushpull}"

echo "I: synchronizing box $(hostname) with the swarm in $MODE mode"
echo W: maybe replace this with an ansible playbook

SESSION="sync-boxes"

clean_old_sessions() {
    echo -n 'tmux kill-session -t "$SESSION:"'
    read nothing
    tmux kill-session -t "$SESSION:"
    echo -n 'sudo tmux kill-session -t "$SESSION:"'
    read nothing
    sudo tmux kill-session -t "$SESSION:"
}

if tmux list-session > /dev/null 2>&1 && tmux has-session -t "$SESSION" 2>/dev/null ; then
	echo >&2 "session $SESSION already exists, control-c to abort..."
	clean_old_sessions
fi

SUDO=""

background() {
    name="$1"
    shift
    if $SUDO tmux has-session -t "$SESSION" 2>/dev/null; then
        echo "I: dispatching '$name' in new window"
        $SUDO tmux new-window -d -t "$SESSION:" -n "$name" "$@"
    else
        echo "I: dispatching '$name' in new session, use 'tmux attach -t $SESSION:' to attach"
        $SUDO tmux new-session -d -s "$SESSION" -n "$name" "$@"
        $SUDO tmux set-option -w -g remain-on-exit on
    fi
}

if echo "$MODE" | grep -qi pull; then

    if ! hostname | grep -q marcos; then
        echo I: first make sure SSH works
        ssh shell.anarc.at true
    fi

    echo I: updating home first
    git -C ~ pull
fi

echo "I: dispatching jobs to tmux"
if echo "$MODE" | grep -qi push; then
    background "mr status" mr -d ~ -m status
fi

if ! hostname | grep -q marcos; then
    background "mail sync" sh -c "systemctl --user start smd-pull smd-push ; notmuch-sync-flagged"
fi

if echo "$MODE" | grep -qi pull; then
    background "flatpak update" flatpak update -y

    background "mr fetch" sh -c "make -j8 -C ~/.ssh all && mr -j8 -m -d ~ fetch -q"
fi

echo I: switching to root jobs
sudo --validate

SUDO=sudo

if echo "$MODE" | grep -qi pull; then
    background "apt dist-upgrade" sh -c "apt update && apt dist-upgrade -y"
    background "schroot-update-all" sh -c "tail -F /var/log/sbuild-update-all.log | grep ^Action &\
sudo sh /usr/share/doc/sbuild/examples/sbuild-update-all 2>/dev/null \
|| sudo sh /usr/share/doc/sbuild/examples/sbuild-debian-developer-setup-update-all ;\
kill %+"
fi
background "backups, control-c to abort" ~/bin/backup-$(hostname)

echo "I: dispatched all jobs, attaching to tmux as user"
sleep 1
tmux attach -t "$SESSION"

echo "I: dispatched all user jobs, attaching as root"
sleep 1
sudo tmux attach -t "$SESSION"

echo "I: synchronization complete, press enter to kill all sessions"
clean_old_sessions
