#!/bin/sh

usage() {
    cat <<EOF
Usage: $0 [ BORG_REPO ]

This script is designed to be a simple, foolproof way to run quick tor
backups for a non-technical user. It expects to be called through
sudo, in a terminal, possible in a shortcut in the GNOME menu bar.

This expects that a first backup has already been performed and guess
the repository path from the borg config. The repository should be
named "$SOMETHING/borg-$(hostname)". This guess procedure will fail
violently if there are more than one repositories matching the
pattern. To skip the guessing, pass the repository path as an
argument.

The only configurable variable is the "DIRECTORIES" below. everything
is hardcoded to sane values. Drop a `.nobackup` file in a directory to
ignore it.
EOF
}

# assume this is a single-user system with everything in /. This might
# also be / /home or else.
DIRECTORIES=/

# guess the repo name.
if [ $# -lt 1 ]; then
    REPO_GUESS=$(grep -h "/borg-$(hostname)" ~/.config/borg/security/*/location)
    echo "using guessed repository path: $BORG_REPO"
else
    BORG_REPO="$1"
fi
BORG_REPO="${1:-$REPO_GUESS}"
export BORG_REPO
# run the backup, prune only on success.
if  borg create --exclude-caches --keep-exclude-tags --one-file-system \
	    --exclude-if-present .nobackup --verbose --stats --progress \
	    -e '/var/cache/*' -e '/tmp/*'  -e '/tmp/*' -e '/var/tmp/*' -e '/home/*/.cache/' \
	    ::'{hostname}-{now}' $DIRECTORIES && \
    borg prune --stats --list \
	    --keep-yearly 1 --keep-monthly 3 --keep-weekly 2 \
	    --keep-within 7d; then
    echo "completed successfully"
else
    echo "completed with errors"
fi
# assume the user started a terminal with just this command and hang
# around until the user confirms.
printf "press enter to exit: "
read _
